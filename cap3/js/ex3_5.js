function calculaRaiz() {
    // cria uma referencoia aos elementos da pagina
    var inNumero = document.getElementById("inNumero");
    var outResposta = document.getElementById("outResposta");
    
    // obtém e converte o conteudo do campo inNumero
    var numero =  Number(inNumero.value)

    // se não preencher ou Not-a-Number (NAN)
    if (numero == 0 || isNaN(numero)) {
        //exibe o alerta
        alert("Informe um número válido...."); 
        //posiciona em inNumero
        inNumero.focus();      
        // retorna
        return;
    }

    // calcula a raiz quadrada do número
    var raiz = Math.sqrt(numero);

    // se valor da variavel raiz igual a este valor arrendodado para baixo ....
    if (raiz == Math.floor(raiz)) {
        // mostra a raiz
        outResposta.textContent = "Raiz: " + raiz;
    } else {
        // senão, exibe mensagem indicando que não há raiz exata
        outResposta.textContent = "Não há raiz exata para o número: " + numero;
    }
}

var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", calculaRaiz);