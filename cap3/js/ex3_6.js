function calculaNotas() {
    // cria uma referencoia aos elementos da pagina
    var inSaque = document.getElementById("inSaque");
    var outNotasCem = document.getElementById("outNotasCem");
    var outNotasCinquenta = document.getElementById("outNotasCinquenta");
    var outNotasDez = document.getElementById("outNotasDez");

    // limpa mensagens (caso segunda execução)
    outNotasCem.textContent = "";
    outNotasCinquenta.textContent = "";
    outNotasDez.textContent = "";


    // obtém e converte o conteudo do campo inSaque
    var saque =  Number(inSaque.value);

    // se não preencher ou Not-a-Number (NAN)
    if (saque == 0 || isNaN(saque)) {
        //exibe o alerta
        alert("Informe valor do saque Corretamente...."); 
        //posiciona em inSaque
        inSaque.focus();      
        // retorna
        return;
    }
    // verifica se saque não é multiplo de 10
    if (saque % 10 != 0) {
        // exibe alerta
        alert("Valoe inválido para notas dísponíveis (R$ 10, 50, 100)")
        // posiciona inSaque
        inSaque.focus();
        return;
    }
    // claula noras de 100,50 e 10
    var notasCem = Math.floor(saque / 100);
    var resto = saque % 100;
    var notasCinquenta = Math.floor(resto / 50);
    var resto = resto % 50;
    var notasDez = Math.floor(resto / 10);
    if (notasCem > 0) {
        outNotasCem.textContent = "Notas de R$ 100: "+ notasCem;
    }
    if (notasCinquenta > 0) {
        outNotasCinquenta.textContent = "Notas de R$ 50: "+ notasCinquenta;
    }
    if (notasDez > 0) {
        outNotasDez.textContent = "Notas de R$ 10: "+ notasDez;
    }
}
// cria referência ao elemento btExibir e associa function ao evento click
var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", calculaNotas);