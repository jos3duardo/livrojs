function verNumero(){
    var inNumero = document.getElementById("inNumero");
    var outResposta = document.getElementById("outResposta");

    var numero = Number(inNumero.value);

    if (numero == 0 || isNaN(numero)) {
        alert("Informe um numero positivo diferente de zero");
        inNumero.focus();
        return;
    }

    if (numero % 2 == 0) {
        outResposta.textContent = numero + " é Par";
    } else {
        outResposta.textContent = numero + " é Impar";
    }

    
}

var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", verNumero);