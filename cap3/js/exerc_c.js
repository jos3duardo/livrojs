/**
 * elabore um programa para simular um paquimetro, o qual leia o valor de moedas
 * depositado em um terminal de estacionamento rotativo. O programa deve informar
 * o tempo de premanencia do veiculo no local e o troco(se existir), como no exemplo
 * da figura 3.9. Se o valor for inferior ao tempo minumo, exiba a mensagem: "Valor
 * Insuficeinte". Considerar os valores/tempos da tabel 3.6(o mazimo é 120 min).
 */

 function verificarMulta(){
     // cria referencia aos elementos da pagina
    var inValor = document.getElementById("inValor");
    var outTempo = document.getElementById("outTempo");
    var outTroco = document.getElementById("outTroco");

    //obtem o conteudo do campo de entrada
    var valor = Number(inValor.value);
    

    // verifica validade do campo valor
    if (valor == 0 || isNaN(valor)) {
        alert("Informe um valor válido de moedas");
        inValor.focus();
        return;
    }

    // caso valor insuficiente
    if (valor < 1.00) {
        alert("Valor Insuficiente (no mínimo, R$ 1.00)");
        inValor.focus();
        return;
    }
  
    // declara variaveis
    var tempo;
    var troco;

    // cria as condições para verificar tempo e troco
    if (valor >= 3.00) {
        tempo = 120;
        troco = valor - 3.00;
    }
    else if (valor >= 1.75) {
        tempo = 60;
        troco = valor - 1.75;
    } else {
        tempo = 30;
        troco = valor - 1.00;
    }

    // exibe as respostas
    outTempo.textContent = "Tempo: " + tempo + "min";
    if (troco > 0) {
        outTroco.textContent = "Troco R$: " + troco.toFixed(2);
    }
}

var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", verificarMulta);