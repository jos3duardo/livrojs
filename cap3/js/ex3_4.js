function calculaFuso() {
    // cria uma referencoia aos elementos da pagina
    var inHoraBrasil = document.getElementById("inHoraBrasil");
    var outHoraFranca = document.getElementById("outHoraFrancao");
    
    // obtém e converte o conteudo do campo InHoraBrasil
    var horaBrasil =  Number(inHoraBrasil.value)

    // se não preencher ou Not-a-Number (NAN)
    if (inHoraBrasil.value == "" || isNaN(horaBrasil)) {
        alert("Informe a hora no Brasil corretamente"); //exibe o alerta
        inHoraBrasil.focus();                           //posiciona em inHoraBrasil
    }

    // calcula o fuso horario na França
    var horaFranca = horaBrasil + 5;

    // se passar das 24 horas na França ....
    if (horaFranca > 24) {
        // subtrai 24
        horaFranca = horaFranca - 24;
    }

    // exibe respota (altera conteudo do elemento outHoraFrancao)
    outHoraFranca.textContent = "Hora na França: " + horaFranca.toFixed(2);
}

var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", calculaFuso);