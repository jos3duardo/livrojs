function calculaMedia(){
    //cria referência aos elementos da página
    var inNome = document.getElementById("inNome");
    var inNota1 = document.getElementById("inNota1");
    var inNota2 = document.getElementById("inNota2");
    var outMedia = document.getElementById("outMedia");
    var outSituacao = document.getElementById("outSituacao");

    //obtém os conteudos dos campos de edição da página
    var nome = inNome.value;
    var nota1 = Number(inNota1.value);
    var nota2 = Number(inNota2.value);

    //calcula  média das notas
    var media = ( nota1 + nota2) / 2;

    //apresenta a média(altera o conteudo do elemento outMedia)
    outMedia.textContent = "Média das Notas: " + media.toFixed(1);

    //cria a condição
    if(media >= 7){
        //sltera o texto e estilo da cor do elemento outSituação
        outSituacao.textContent = "Parabéns "+nome+" ! Você foi aprovado(a)";
        outSituacao.estyle.color = "blue";
    }else if(media >=4){
        outSituacao.textContent = "Atenção "+nome+" ! Você esta de exame";
        outSituacao.estyle.color = "green";
    }
    
    else{
        outSituacao.textContent = "Ops "+nome+" .... Você foi reprovado(a)";
        outSituacao.estyle.color = "red";
    }
}

//cira uma referencia ao elemento btResultado (botão)
var btResultado = document.getElementById("btResultado");
//registra um evento associado ao botão para carregar uma função
btResultado.addEventListener("click", calculaMedia);