/**elaboew um programa que leia a velocidade condutor em uma estrada e a velocidade de um conduto
 * Se a velocidade for inferior ou igual a condutor, exiba "Sem Multa"
 * Se a velocidade for de até 20% maior que a condutor, exiba "Multa Leve"
 * E se a velocidade for superior a 20% da velocidade condutor, exiba "Multa Grave""
 * 
 */function verNumero(){
    var inVelocidadecondutor = document.getElementById("inVelocidadePermitida");
    var inVelocidadeCondutor = document.getElementById("inVelocidadeCondutor");
    var outResposta = document.getElementById("outResposta");

    var velocidadePermitida = Number(inVelocidadePermitida.value);
    var velocidadeCondutor = Number(inVelocidadeCondutor.value);
    var leve = velocidadePermitida * 1.2;

    if (velocidadePermitida == 0 || isNaN(velocidadePermitida)) {
        alert("Informe velocidade da via corretamente");
        inVelocidadePermitida.focus();
        return;
    }


    if (velocidadeCondutor == 0 || isNaN(velocidadeCondutor)) {
        alert("Informe velocidade do condutor corretamente");
        inVelocidadeCondutor.focus();
        return;
    }
    
    if (velocidadeCondutor <= velocidadePermitida) {
        outResposta.textContent = "Situação: Sem Multa";
    }
    if (velocidadeCondutor > velocidadePermitida && velocidadeCondutor <= leve) {
        outResposta.textContent = "Situação: Multa Leve";
    }
    if (velocidadeCondutor > leve) {
        outResposta.textContent = "Situação: Multa Grave";
    }
    
    

    
    
}

var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", verNumero);