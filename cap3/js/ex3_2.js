function calculaPeso() {
    //cria referencia aos elementos manipulados pela function
    var inNome = document.getElementById("inNome");
    var rbMasculino = document.getElementById("rbMasculino");
    var rbFeminino = document.getElementById("rbFeminino");
    var inAltura = document.getElementById("inAltura");
    var outResposta = document.getElementById("outResposta");
    //obtém o conteudo dos campos de adição da página
    var nome = inNome.value;
    var masculino = rbMasculino.checked;
    var feminino = rbFeminino.checked;
    var altura = Number(inAltura.value);
    //verifica se nome foi preenchido e sexo selecionado
    if (nome == "" || (masculino == false && feminino == false)) {
        alert("Por favor, informe o nome e selecione o sexo....");
        inNome.focus(); //posiciona (joga o foco) no campo de edição inNome
        return;
    }
    //se altura vazio (0) ou Nan: Not-a-Number (um texto for informado, por exemplo)
    if (altura == 0 || isNaN(altura)) {
        alert("Por favor, informe a altura corretamente...");
        inAltura.focus();
        return;
    }
    //se masculino (significa se masculino == true)
    if (masculino) {
        //eleva ao quadrado
        var peso = 22 * Math.pow(altura, 2);
    }
    else {
        //eleva ao quadrado
        var peso = 21 * Math.pow(altura, 2);
    }
    //apresenta a resposta (altura o conteudo da linha outResposta)
    outResposta.textContent = nome + ": Seu peso ideal é " + peso.toFixed(3) + " kg";
}
//cria referencia ao elemento btCalcular e registra evento associativo a calcularPeso
var btCalcular = document.getElementById("btCalcular");
btCalcular.addEventListener("click", calculaPeso);
function limparCampos() {
    //Limpa os campos dos elementos
    document.getElementById("inNome").value = " ";
    document.getElementById("rbMasculino").checked = false;
    document.getElementById("rbFeminino").checked = false;
    document.getElementById("inAltura").value = " ";
    document.getElementById("outResposta").textContent = " ";
    //posiciona (joga o foco) no elemento inNome
    document.getElementById("inNome").focus();
}
var btLimpar = document.getElementById("btLimpa");
btLimpar.addEventListener("click", limparCampos);