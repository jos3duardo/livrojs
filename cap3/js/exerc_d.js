/**
 * elabore um programa para simular um paquimetro, o qual leia o valor de moedas
 * depositado em um terminal de estacionamento rotativo. O programa deve informar
 * o tempo de premanencia do veiculo no local e o troco(se existir), como no exemplo
 * da figura 3.9. Se o valor for inferior ao tempo minumo, exiba a mensagem: "Valor
 * Insuficeinte". Considerar os valores/tempos da tabel 3.6(o mazimo é 120 min).
 */

 function verificarLado(){
     // cria referencia aos elementos da pagina
    var inLadoA = document.getElementById("inLadoA");
    var inLadoB = document.getElementById("inLadoB");
    var inLadoC = document.getElementById("inLadoC");
    var outExibir = document.getElementById("outExibir");
    var outTipo = document.getElementById("outTipo");
 
    //obtem o conteudo do campo de entrada
    var ladoA = Number(inLadoA.value);
    var ladoB = Number(inLadoB.value);
    var ladoC = Number(inLadoC.value);

    // verifica validade dos campos
    if (ladoA == 0 || isNaN(ladoA)) {
        alert("Informe um valor válido para o campo Lado A");
        inLadoA.focus();
        return;
    }
    if (ladoB == 0 || isNaN(ladoB)) {
        alert("Informe um valor válido para o campo Lado B");
        inLadoB.focus();
        return;
    }
    if (ladoC == 0 || isNaN(ladoC)) {
        alert("Informe um valor válido para o campo Lado C");
        inLadoC.focus();
        return;
    }
    if (ladoA > ladoB + ladoC || ladoB > ladoA + ladoC || ladoC > ladoA + ladoB  ) {
        outExibir.textContent = "Lados não podem formar um triangulo";
    }else{
        outExibir.textContent = "Lados podem formar um triangulo";
        if (ladoA == ladoB && ladoA == ladoC) {
            outTipo.textContent = "Tipo Equilátero";
        } else if(ladoA == ladoB || ladoB == ladoC || ladoB == ladoC){
            outTipo.textContent = "Tipo Isósceles";
        }else{
        outTipo.textContent = "Tipo Escaleno";
        }
    }
}

var btExibir = document.getElementById("btExibir");
btExibir.addEventListener("click", verificarLado);