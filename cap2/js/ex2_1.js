//declara a função mostrarOla
function mostrarOla(){
    //obtem o conteudo do campo (com id=) nome
    var nome = document.getElementById("nome").value;
    //exibe  no paragrafo (resposta): "Olá " e o nome informado
    document.getElementById("resposta").innerHTML = "Olá " + nome;
}
//cria uma referencia ao botão (com id=) mostrar
var mostrar = document.getElementById("mostrar");
//registra para o botão "mostrar" um ouvinte para o evento click
//que ao ssser clicado irá chamar a função mostrarOla
mostrar.addEventListener("click", mostrarOla);
