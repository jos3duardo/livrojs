/* Elabora um programa para uma lan house de um aeroporto.
* O programa deve ler o vaor de cada 15 minutos de uso de um computador
* e o tempo de uso por um cliente em minutos.
* Informe o valor a ser pago pelo cliente, sabendo que as frações extras
* de 15 minutos devem ser cobradas de forma integral
*/

function calcularPreco(){
    // cria refêrncia aos elementos manupulados pelo programa
    var inValor = document.getElementById("inValor");
    var inTempo = document.getElementById("inTempo");
    var outValor = document.getElementById("outValor");
    
    // obtém conteúdo dos campos de entrada
    var valor = Number(inValor.value);
    var tempo = Number(inTempo.value);
    
    // calcula o valor a ser pago
    
    var totalTempoUtilizado =Math.ceil(tempo / 15);
    var totalTempo = totalTempoUtilizado * valor;
    //var totalCliente = (quilo / 1000) * consumo;
    
    // altera o conteudo da linha de resposta
    outValor.textContent = "Valor a pagar R$: " + totalTempo.toFixed(2);
}
// cria uma referencia ao elemento btCalcular
var btCalcular = document.getElementById("btCalcular");
// registra um evento associado ao botão, para carregar uma função
btCalcular.addEventListener("click", calcularPreco);
