/** Um super mercado esta com uma promoção
 * Para aumentar duas vendas no setor de higiene
 * cada etiqueta de produto deve exibir uma mensagem
 * anunciando 50% de desconto (para um item) na compra de 
 * três unidades do produto
 * Elaborar um programa que leia a descrição e preço de um 
 * produto. Após apresente uma mesagem indicando a promoção
 * 
 */
function calcularPreco(){
    // cria refêrncia aos elementos manupulados pelo programa
    var inProduto = document.getElementById("inProduto");
    var inPreco = document.getElementById("inPreco");
    var outValor = document.getElementById("outValor");
    var outProduto = document.getElementById("outProduto");
    
    // obtém conteúdo dos campos de entrada
    var produto = inProduto.value;
    var preco = Number(inPreco.value);
    
    // calcula o valor a ser pago
    var desconto = preco / 2;
    var promocao = (preco * 2) + desconto;
    
    // altera o conteudo da linha de resposta
    outProduto.textContent = produto + " - Promoção: Leve 3 por apenas R$: "+ promocao.toFixed(2);
    outValor.textContent = "O 3ª produto custa apenas R$: " + desconto.toFixed(2);
}
// cria uma referencia ao elemento btCalcular
var btCalcular = document.getElementById("btCalcular");
// registra um evento associado ao botão, para carregar uma função
btCalcular.addEventListener("click", calcularPreco);