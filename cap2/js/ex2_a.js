/*problematica: 
* Uma Farmacioa esta com uma promoção - Na compra de duas unidades
* de um mesmo medicamento, o cleinte recebe como desconto os 
* centavos do valot total.
*/
function calcularPreco(){
    // cria refêrncia aos elementos manupulados pelo programa
    var inMedicamento = document.getElementById("inMedicamento");
    var inPreco = document.getElementById("inPreco");
    var outValor = document.getElementById("outValor");
    
    // obtém conteúdo dos campos de entrada
    var medicamento =inMedicamento.value;
    var preco = Number(inPreco.value);
    
    
    // calcula o valor a ser pago
    var totalDesconto = (preco * 2)  % 2;
    var totalPagamento = (preco * 2) - totalDesconto;
    
    
    // altera o conteudo da linha de resposta
    outMedicamento.textContent = "Promoção de " + medicamento;
    outValor.textContent = "Leve 2 por apenas R$: " + totalPagamento.toFixed(2);
}
// cria uma referencia ao elemento btCalcular
var btCalcular = document.getElementById("btCalcular");
// registra um evento associado ao botão, para carregar uma função
btCalcular.addEventListener("click", calcularPreco);

