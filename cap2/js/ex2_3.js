/**Elabore um programa para uma revenda de veiculos.
 * O programa deve ler o mpdelo e preço do veiculo.
 * Apresentar como resposta o balor da entrada(50%) 
 * e o saldo em 12x.
 */
function mostrarPromocao(){
    // cria refêrncia aos elementos manupulados pelo programa
    var inVeiculo = document.getElementById("inVeiculo");
    var inPreco = document.getElementById("inPreco");
    var outVeiculo = document.getElementById("outVeiculo");
    var outEntrada = document.getElementById("outEntrada");
    var outParcela = document.getElementById("outParcela");

    // obtém conteúdo dos campos de entrada
    var veiculo = inVeiculo.value;
    var preco = inPreco.value;

    // calcula o valor de entrada e das parcela
    var entrada = preco * 0.50;
    var parcela = (preco * 0.50) / 12;

    // altera o conteudo dos paragrados de resposta
    outVeiculo.textContent = "Promoção: " + veiculo;
    outEntrada.textContent = "Entrada de R$: " + entrada.toFixed(2);
    outParcela.textContent = "+ 12 de R$: " + parcela.toFixed(2);
}
// cria uma referencia ao elemento btVerPromocao
var btVerPromocao = document.getElementById("btVerPromocao");
btVerPromocao.addEventListener("click", mostrarPromocao);