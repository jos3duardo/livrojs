function converteDuracao(){
    //cria referencia aos elemntos da pagina
    var inTitulo = document.getElementById("inTitulo");
    var inDuracao = document.getElementById("inDuracao");
    var outTitulo = document.getElementById("outTitulo");
    var outResposta = document.getElementById("outResposta");

    //obtem conteudos dos campos de entrada
    var titulo = inTitulo.value;
    var duracao = inDuracao.value;

    //aredonda para baixo o resultada da divisão
    var horas = Math.floor(duracao / 60);
    //obtem o resto da divisão
    var minutos = duracao % 60;

    //altera o conteúdo dos paragrados de resposta
    outTitulo.textContent = titulo;
    outResposta.textContent = horas + " hora(s) e " + minutos + "minuto(s)";
}
//cria a referencia ao elemento btnConverter (botão)
var btConverter = document.getElementById("btConverter");
//registra um evento associativo ao botão, para carregar um função
btConverter.addEventListener("click", converteDuracao);