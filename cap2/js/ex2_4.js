function calcularPreco(){
    // cria refêrncia aos elementos manupulados pelo programa
    var inQuilo = document.getElementById("inQuilo");
    var inConsumo = document.getElementById("inConsumo");
    var outValor = document.getElementById("outValor");
    
    // obtém conteúdo dos campos de entrada
    var consumo = Number(inConsumo.value);
    var quilo = Number(inQuilo.value);
    
    // calcula o valor a ser pago
    var totalCliente = (quilo / 1000) * consumo;
    
    // altera o conteudo da linha de resposta
    outValor.textContent = "Valor a pagar R$: " + totalCliente.toFixed(2);
}
// cria uma referencia ao elemento btCalcular
var btCalcular = document.getElementById("btCalcular");
// registra um evento associado ao botão, para carregar uma função
btCalcular.addEventListener("click", calcularPreco);