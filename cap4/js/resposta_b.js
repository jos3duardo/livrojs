function contaChinchila(){
    // cria referencia aos elementos da pagina
    var inChinchila = document.getElementById("inChinchila");
    var inAnos = document.getElementById("inAnos");
    var outResposta = document.getElementById("outResposta");
    
    // obtem os dados informadors
    var chinchila = Number(inChinchila.value);
    var anos = Number(inAnos.value);
  
        // verifica se os campos estão preenchidos corretamente
        if(chinchila < 2 || isNaN(chinchila) || anos == 0 || isNaN(anos)){
            alert("Informe CorretamentASDASDASDe os dados.");
            inChinchila.focus();
            return;
        }
    
        // variavel que vai acumular o anos de chinchilas
        var resposta = "";
        var numeroChinchilas = chinchila;
    
        for (var i = 1; i <= anos; i++) {
           
            resposta = resposta + i + "ª Ano: " + numeroChinchilas+ " chinchilas \n";
            numeroChinchilas = numeroChinchilas * 3;
        }
        
        outResposta.textContent = resposta;
    }
    
    // referencia elemento e após associa function ao evento click
    var btVerifica = document.getElementById("btVerifica");
    btVerifica.addEventListener("click", contaChinchila);