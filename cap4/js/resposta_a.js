function contaFruta(){
// cria referencia aos elementos da pagina
var inFruta = document.getElementById("inFruta");
var inNumero = document.getElementById("inNumero");
var outResposta = document.getElementById("outResposta");

// obtem os dados informadors
var numero = Number(inNumero.value);
var fruta = inFruta.value;

    // verifica se os campos estão preenchidos corretamente
    if(fruta == "" || numero == 0 || isNaN(numero)){
        alert("Informe Corretamente os dados.");
        inFruta.focus();
        return;
    }

    // variavel que vai acumular o numero de frutas
    var resposta = "";

    for (var i = 1; i < numero; i++) {
        resposta = resposta + fruta + " + ";
    }

    resposta = resposta + fruta;
    
    outResposta.textContent = resposta;
}

// referencia elemento e após associa function ao evento click
var btVerifica = document.getElementById("btVerifica");
btVerifica.addEventListener("click", contaFruta);