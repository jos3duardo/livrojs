function verificaPerfeito(){
    // referencia os elementos da pagina
    var inNumero = document.getElementById("inNumero");
    var outDivisores = document.getElementById("outDivisores");
    var outPerfeito = document.getElementById("outPerfeito");

    // pega numero
    var numero = Number(inNumero.value);

    // verifica se os dados de entrada estão corretos
    if (numero == 0 || isNaN(numero)) {
        alert("Informe os dados corretamente");
        inNumero.focus();
        return;
    }

    
  // como 1 é um divisor universal, já iniciamos com ele
  var divisores = "Divisores do " + numero + ": 1";
  var soma = 1;

  // percorre os possíveis divisores e acumula
  for (var i = 2; i <= numero / 2; i++) {
    if (numero % i == 0) {
      divisores = divisores + ", " + i;  // vírgula + i (evita última vírgula) 
      soma = soma + i;
    }
  }
  divisores = divisores + " (Soma: " + soma + ")";

  // altera o conteúdo de outDivisores
  outDivisores.textContent = divisores;

  // verifica se é perfeito e exibe resposta na tag outResposta
  if (numero == soma) {
    outResposta.textContent = numero + " É um Número Perfeito";
  } else {
    outResposta.textContent = numero + " Não É um Número Perfeito";
  }

}

    // referencia elemento e após associa function ao evento click
    var btVerifica = document.getElementById("btVerifica");
    btVerifica.addEventListener("click", verificaPerfeito);