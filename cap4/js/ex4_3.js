function mostrarNumero(){

    // solicita o número e repete a leitura enquanto invalido
    do {
        var num = Number(prompt("Número: "));
        if (num == 0 || isNaN(num)) {
            alert("Digite um número válido....");
        }
    } while (num == 0 || isNaN(num));


    // declara e inicializa variavel que irá exibir pares
    var pares = "Pares entre 1 e " + num + ": ";

    // isola o primeiro par (para evirar útima virgula)
    if (num > 1) {
        pares = pares + 2;
    }

    // laço para acumular pares (inicia em 4, pois o 2 ja foi atribuido)
    for (var i = 4; i <= num; i = i + 2) {
        pares = pares + ", " + i;
    }
    // exibe lista dos números pares
    alert(pares);
}


var btMostrar = document.getElementById("btMostrar");
btMostrar.addEventListener("click", mostrarNumero);