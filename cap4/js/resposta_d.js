function criarEstrelas(){
    // referencia os elementos da pagina
    var inLinhas = document.getElementById("inLinhas");
    var outResposta = document.getElementById("outResposta");
    

    // obtem os dados informadors
    var linhas = Number(inLinhas.value);

    // valida os dados inseridos
    if (linhas == 0 || isNaN(linhas)) {
        alert("Infome os dados corretametne");
        inLinhas.focus();
        return;
    }

    // variavel que vai armazenar o linhas de estrelas
    var estrelas = "";

    for (var i = 1; i <= linhas; i++) {
        for (var j = 1; j <= i; j++) {
            estrelas = estrelas + "*";     
        }   
        estrelas = estrelas + "\n";
    }

outResposta.textContent = estrelas;

}
var btVerifica = document.getElementById("btVerifica");
btVerifica.addEventListener("click", criarEstrelas);