function mostrarTabuada(){
    // cria referencia aos elementos da pagina
    var inNumero = document.getElementById("inNumero");
    var outResposta = document.getElementById("outResposta");

    var numero = Number(inNumero.value);

    // valida o numero
    if (numero ==0 || isNaN(numero)) {
        alert("Informe um número valido")
        inNumero.focus();
        return;
    }

    // inicializa a variavel resposta
    var resposta = "Entre " + numero + " e 1: ";

    // cria um for decrescente
    for (var i = numero; i > 1; i = i -1) {
        // variavel resposta vai acumulando os novos conteudos
        resposta = resposta   + i + ", " ;
    }
    resposta = resposta + i + ".";

    // o conteudo da tag pre é alterado para exibir a tabuada do numero
    outResposta.textContent = resposta;
}

// cria referencia ao botão e após associa a function ao evento click
var btMostrar = document.getElementById("btMostrar");
btMostrar.addEventListener("click", mostrarTabuada);