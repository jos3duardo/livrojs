function mostrarTabuada(){
    // cria referencia aos elementos da pagina
    var inNumero = document.getElementById("inNumero");
    var outTabuada = document.getElementById("outTabuada");

    var numero = Number(inNumero.value);

    // valida o numero
    if (numero ==0 || isNaN(numero)) {
        alert("Informe um número valido")
        inNumero.focus();
        return;
    }

    // cria uma variavel do tipo String, que ira concatenas a resposta
    var resposta = "";

    // cria um laço de repetição
    for (var i = 1; i <= 10; i++) {
        // variavel resposta vai acumulando os novos conteudos
        resposta = resposta + numero + " x " + i + " = " + numero * i + "\n";
    }

    // o conteudo da tag pre é alterado para exibir a tabuada do numero
    outTabuada.textContent = resposta;
}

// cria referencia ao botão e após associa a function ao evento click
var btMostrar = document.getElementById("btMostrar");
btMostrar.addEventListener("click", mostrarTabuada);