function verificaPrimo(){
    // cria referencias aos elementos da página
    var inNumero = document.getElementById("inNumero");
    var outResposta = document.getElementById("outResposta");

    // obtem o numero informado
    var numero = Number(inNumero.value);

    // verifica se os campos estão preenchidos corretamente
    if (numero == 0 || isNaN(numero)){
        alert("Número Inválido");
        inNumero.focus();
        return;
    }

    // declara e inicializa uma variavel do tipo flag
    var temDivisor = 0;
    
    // percorre todos os possiveis divisores do numero
    for(var i = 2; i <= numero / 2; i++){
        if (numero % i == 0){
            temDivisor = 1;  //muda o flag
            break; //sai da repetição
        }
    }

    // se numero >1 e não possui divisor
    if(numero > 1 && !temDivisor){
        outResposta.textContent = numero + " É primo";
    }else{
        outResposta.textContent = numero + " Não é primo";
    }
}

// referencia elemento e após associa function ao evento click
var btVerificarPrimo = document.getElementById("btVerifica");
btVerificarPrimo.addEventListener("click", verificaPrimo);